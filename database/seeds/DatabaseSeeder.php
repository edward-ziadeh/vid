<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      foreach(range(1,1000) as $index){

        DB::table('values')->insert([
          'strong' => $faker->numberBetween($min = 1, $max = 6),
          'value1' => $faker->numberBetween($min = 1, $max = 37),
          'value2' => $faker->numberBetween($min = 1, $max = 37),
          'value3' => $faker->numberBetween($min = 1, $max = 37),
          'value4' => $faker->numberBetween($min = 1, $max = 37),
          'value5' => $faker->numberBetween($min = 1, $max = 37),
          'value6' => $faker->numberBetween($min = 1, $max = 37)
        ]);

      }
    }
}
