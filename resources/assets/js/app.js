/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('valuecomponent', require('./components/ValueComponent.vue'));

const app = new Vue({
    el: '#app',

    data: {
        values: [],
        values1: [],
        oneValue:[],
        limit: 1000,
        counter: 0
    },
    methods: {
        getValues: function getValues() {
            return axios.get('/api/data/' + this.counter).then(({ data }) => this.values = data);
        },

        getValues1: function getValues1() {
            return axios.get('/api/data/' + (this.limit - this.counter)).then(({ data }) => this.values1 = data);
        },

        getOneValue: function getOneValue(){
            return axios.get('/api/onevalue/' + this.counter).then(({ data }) => this.oneValue = data);
        }

    },
    beforeUpdate() {
        console.log(this.counter);
    },

    beforeDestroy() {
        this.counter = null;
        delete this.counter;
    },

    mounted() {
        setInterval(() => {

            if (this.counter >= this.limit)
                this.counter = 0;
            this.counter++;
            axios.all([this.getValues(), this.getValues1(), this.getOneValue()])
                .then(axios.spread(function(acct, perms) {
                    console.log('Completed');
                }));
        }, 100);
    }
});
