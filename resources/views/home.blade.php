@extends('layouts.master')
@section('content')

<div class="col-md-12 alert alert-success">
  <strong>Multi requests</strong> using VUEJS and AXIOS, eache time a request fired calling the database to pull data.
</div>
<div class="col-md-6">
  <div v-for="value in values" class="card" style="width: 30rem; margin-bottom: 20px;">
    <div class="card-header">
      First request - Your Luck number
    </div>
    <div class="card-body">
      <h5 class="card-title">Number ID: @{{value.id}}</h5>
      <p class="card-text">Strong Number <span class="lotoCircle grey">@{{value.strong}}</span></p>

      <h2><span class="lotoCircle red">@{{value.value1}}</span></h2>
      <h2><span class="lotoCircle blue">@{{value.value2}}</span></h2>
      <h2><span class="lotoCircle green">@{{value.value3}}</span></h2>
      <h2><span class="lotoCircle pink">@{{value.value4}}</span></h2>
      <h2><span class="lotoCircle grey">@{{value.value5}}</span></h2>
      <h2><span class="lotoCircle grey">@{{value.value6}}</span></h2>
    </p>
  </div>
  </div>

  <div v-for="value in values1" class="card" style="width: 30rem;">
    <div class="card-header">
      Second request - Your Luck number
    </div>
    <div class="card-body">
      <h5 class="card-title">Number ID: @{{value.id}}</h5>
      <p class="card-text">Strong Number <span class="lotoCircle grey">@{{value.strong}}</span></p>

      <h2><span class="lotoCircle red">@{{value.value1}}</span></h2>
      <h2><span class="lotoCircle blue">@{{value.value2}}</span></h2>
      <h2><span class="lotoCircle green">@{{value.value3}}</span></h2>
      <h2><span class="lotoCircle pink">@{{value.value4}}</span></h2>
      <h2><span class="lotoCircle grey">@{{value.value5}}</span></h2>
      <h2><span class="lotoCircle grey">@{{value.value6}}</span></h2>
    </p>
  </div>
</div>
</div>

<div class="col-md-6">
  <h2>Third request</h2>
  <div v-for="value in oneValue">
    <h1><span class="lotoCircle red">@{{value.strong}}</span></h1>
  </div>
</div>
@endsection
