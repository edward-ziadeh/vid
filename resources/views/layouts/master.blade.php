<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'My App') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  @include('layouts.header')
  <div id="app" class="container">
    <div class="row">
      @yield('content')
      @yield('content1')
    </div>
  </div>
  @include('layouts.footer')
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
