<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Value;
class ValuesController extends Controller
{
    public function index(){
      $values = Value::All();

      return view('home', compact(['values']));
    }

    public function getData($id){

      $values = Value::where('id',$id)->get();

      return $values;
    }
    public function getOneValue($id){
      $value = Value::select('strong')->where('id',$id)->get();

      return $value;
    }
}
